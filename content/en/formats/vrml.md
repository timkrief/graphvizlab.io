---
name: VRML
params:
- vrml
---
Outputs graphs in the [VRML](http://www.vrml.org/) format.
To get a 3D embedding, nodes must have a [z](attrs.html#d:z)
attribute. These can either be supplied as part of the input graph, or
be generated by neato provided [dim](attrs.html#d:dim)`=3`
and at least one node has a **z** value.

Line segments are drawn as cylinders.
In general, VRML output relies on having the PNG library to produce images
used to texture-fill the node shapes. However, if
[shape](attrs.html#d:shape)`=point`,
a node is drawn as a 3D sphere.
