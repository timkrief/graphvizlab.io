---
name: Scalable Vector Graphics
params:
- svg
- svgz
---
Produce [SVG](http://www.adobe.com/svg/) output,
the latter in compressed format.

See [Note](#ID).
