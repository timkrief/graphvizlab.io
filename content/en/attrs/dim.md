---
defaults:
- '2'
flags:
- neato
- fdp
- sfdp
minimums:
- '2'
name: dim
types:
- int
used_by: G
---
Set the number of dimensions used for the layout.

The maximum value allowed is `10`.
