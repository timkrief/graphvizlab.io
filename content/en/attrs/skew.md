---
defaults:
- '0.0'
flags: []
minimums:
- '-100.0'
name: skew
types:
- double
used_by: "N"
---
Skew factor for [`shape`](#d:shape)`=polygon`.

Positive values skew top of polygon to right; negative to left.
