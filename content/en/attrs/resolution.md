---
defaults:
- '96.0'
- '0.0'
flags:
- bitmap output
- svg
minimums: []
name: resolution
types:
- double
used_by: G
---
Synonym for [`dpi`](#d:dpi).
