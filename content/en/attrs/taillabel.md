---
defaults:
- '""'
flags: []
minimums: []
name: taillabel
types:
- lblString
used_by: E
---
Text label to be placed near tail of edge.

See [limitation](#h:undir_note).
