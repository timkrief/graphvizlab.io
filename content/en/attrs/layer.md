---
defaults:
- '""'
flags: []
minimums: []
name: layer
types:
- layerRange
used_by: ENC
---
Specifies layers in which the node, edge or cluster is present.
