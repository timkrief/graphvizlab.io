---
defaults: []
flags:
- write
minimums: []
name: rects
types:
- rect
used_by: "N"
---
Rectangles for fields of records, [in points](#points).
