---
defaults:
- '""'
flags:
- map
- svg
minimums: []
name: tailURL
types:
- escString
used_by: E
---
If defined, `tailURL` is output as part of the tail label of the
edge.

Also, this value is used near the tail node, overriding any
[`URL`](#d:URL) value.

See [limitation](#h:undir_note).
